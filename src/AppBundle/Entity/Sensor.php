<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sensor
 *
 * @ORM\Table(name="sensor")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SensorRepository")
 */
class Sensor
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="SensorHistory", mappedBy="sensor")
     */
    private $sensorHistory;

    /**
     * @ORM\ManyToOne(targetEntity="FreezerRoom", inversedBy="sensor")
     * @ORM\JoinColumn(name="freezerRoom_id", referencedColumnName="id")
     */
    private $freezerRoom;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sensorHistory = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Sensor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add sensorHistory
     *
     * @param \AppBundle\Entity\SensorHistory $sensorHistory
     *
     * @return Sensor
     */
    public function addSensorHistory(\AppBundle\Entity\SensorHistory $sensorHistory)
    {
        $this->sensorHistory[] = $sensorHistory;

        return $this;
    }

    /**
     * Remove sensorHistory
     *
     * @param \AppBundle\Entity\SensorHistory $sensorHistory
     */
    public function removeSensorHistory(\AppBundle\Entity\SensorHistory $sensorHistory)
    {
        $this->sensorHistory->removeElement($sensorHistory);
    }

    /**
     * Get sensorHistory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSensorHistory()
    {
        return $this->sensorHistory;
    }

    /**
     * Set freezerRoom
     *
     * @param \AppBundle\Entity\FreezerRoom $freezerRoom
     *
     * @return Sensor
     */
    public function setFreezerRoom(\AppBundle\Entity\FreezerRoom $freezerRoom = null)
    {
        $this->freezerRoom = $freezerRoom;

        return $this;
    }

    /**
     * Get freezerRoom
     *
     * @return \AppBundle\Entity\FreezerRoom
     */
    public function getFreezerRoom()
    {
        return $this->freezerRoom;
    }
}
