<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FreezerRoom
 *
 * @ORM\Table(name="freezer_room")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FreezerRoomRepository")
 */
class FreezerRoom
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Sensor", mappedBy="freezerRoom")
     */
    private $sensor;

    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="freezerRoom")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sensor = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return FreezerRoom
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add sensor
     *
     * @param \AppBundle\Entity\Sensor $sensor
     *
     * @return FreezerRoom
     */
    public function addSensor(\AppBundle\Entity\Sensor $sensor)
    {
        $this->sensor[] = $sensor;

        return $this;
    }

    /**
     * Remove sensor
     *
     * @param \AppBundle\Entity\Sensor $sensor
     */
    public function removeSensor(\AppBundle\Entity\Sensor $sensor)
    {
        $this->sensor->removeElement($sensor);
    }

    /**
     * Get sensor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSensor()
    {
        return $this->sensor;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     *
     * @return FreezerRoom
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
