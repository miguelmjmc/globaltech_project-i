<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SensorHistory
 *
 * @ORM\Table(name="sensor_history")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SensorHistoryRepository")
 */
class SensorHistory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="temperature", type="string", length=255)
     */
    private $temperature;

    /**
     * @var string
     *
     * @ORM\Column(name="setPoint", type="string", length=255)
     */
    private $setPoint;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Sensor", inversedBy="sensorHistory")
     * @ORM\JoinColumn(name="sensor_id", referencedColumnName="id")
     */
    private $sensor;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set temperature
     *
     * @param string $temperature
     *
     * @return SensorHistory
     */
    public function setTemperature($temperature)
    {
        $this->temperature = $temperature;

        return $this;
    }

    /**
     * Get temperature
     *
     * @return string
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * Set setPoint
     *
     * @param string $setPoint
     *
     * @return SensorHistory
     */
    public function setSetPoint($setPoint)
    {
        $this->setPoint = $setPoint;

        return $this;
    }

    /**
     * Get setPoint
     *
     * @return string
     */
    public function getSetPoint()
    {
        return $this->setPoint;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return SensorHistory
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set sensor
     *
     * @param \AppBundle\Entity\Sensor $sensor
     *
     * @return SensorHistory
     */
    public function setSensor(\AppBundle\Entity\Sensor $sensor = null)
    {
        $this->sensor = $sensor;

        return $this;
    }

    /**
     * Get sensor
     *
     * @return \AppBundle\Entity\Sensor
     */
    public function getSensor()
    {
        return $this->sensor;
    }
}
