<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="FreezerRoom", mappedBy="client")
     */
    private $freezerRoom;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="client")
     * @ORM\JoinTable(name="user_client")
     */
    private  $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->freezerRoom = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add freezerRoom
     *
     * @param \AppBundle\Entity\FreezerRoom $freezerRoom
     *
     * @return Client
     */
    public function addFreezerRoom(\AppBundle\Entity\FreezerRoom $freezerRoom)
    {
        $this->freezerRoom[] = $freezerRoom;

        return $this;
    }

    /**
     * Remove freezerRoom
     *
     * @param \AppBundle\Entity\FreezerRoom $freezerRoom
     */
    public function removeFreezerRoom(\AppBundle\Entity\FreezerRoom $freezerRoom)
    {
        $this->freezerRoom->removeElement($freezerRoom);
    }

    /**
     * Get freezerRoom
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFreezerRoom()
    {
        return $this->freezerRoom;
    }
}
